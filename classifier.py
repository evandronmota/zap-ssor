# Import packages
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import numpy as np
import pandas as pd
from sklearn import metrics
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

videos = {
    "ecologia_0" : "https://www.youtube.com/watch?v=w8zHbA3CSBo",
    "ecologia_1" : "https://www.youtube.com/watch?v=TsclSi3nNsI",
    "cadeia_0" : "https://www.youtube.com/watch?v=FgwWb_WVRYk",
    "cadeia_1" : "https://www.youtube.com/watch?v=9cOQZGRbJYI",
    "relacao_0" : "https://www.youtube.com/watch?v=CBN9WgeGkTA",
    "relacao_1" : "https://www.youtube.com/watch?v=cpmcIciaIWc"
}

video_id = [ 
    "ecologia_0", "ecologia_1", "cadeia_0", "cadeia_1", "relacao_0", "relacao_1"
]

train = pd.read_csv('perfil.csv')
train['video'],video_train = pd.factorize(train['video'])

test = pd.read_csv('perfil.csv')
test['video'],video_test = pd.factorize(test['video'])

xtrain = train.drop('video', 1)
ytrain = train.loc[:, 'video']

xtest = test.drop('video', 1)
ytest = test.loc[:, 'video']

# print(xtrain)
# print(ytrain)

model = GaussianNB()
model.fit(xtrain, ytrain)

# count_misclassified = (ytest != pred).sum()
# print('Misclassified samples: {}'.format(count_misclassified))
# accuracy = metrics.accuracy_score(ytest, pred)
# print('Accuracy: {:.2f}'.format(accuracy))

# mat = confusion_matrix(pred, ytest)
# names = np.unique(["Ecologia - v1", "Ecologia - v2", "Cadeia Alim. - v1", "Cadeia Alim. - v2", 
#                     "R. interespécies - v1", "R. Interespécies - v2"])
# sns.heatmap(mat, square=True, annot=True, fmt='d', cbar=False,
#             xticklabels=names, yticklabels=names)
# plt.xlabel('Truth')
# plt.ylabel('Predicted')

# plt.show()

p0, p1, p2 = 0.71, 0.34, 0.47
test = {'p0' : p0, 'p1' : p1, 'p2' : p2}
xtest = xtest.append(test, ignore_index = True)

pred = model.predict(xtest)

print("{")
print("\t\"id\" : \"jorgealmes\",\n\t\"profile\" : {\n\t\t0.71, 0.34, 0.47\n\t}")
print("}")

print("{\n\t\"response\" : {\n\t\t\"video_id\" : \"", video_id[pred[-1]],"\"\n\t\t\"url\" : \"", videos[video_id[pred[-1]]],"\"\n\t}\n}", sep='')

profile = input("get profile: ")
p0, p1, p2 = profile.split()
p0, p1, p2 = float(p0), float(p1), float(p2)

test = {'p0' : p0, 'p1' : p1, 'p2' : p2}
xtest = xtest.append(test, ignore_index = True)

pred = model.predict(xtest)

print("{\n\t\"response\" : {\n\t\t\"video_id\" : \"", video_id[pred[-1]],"\"\n\t\t\"url\" : \"", videos[video_id[pred[-1]]],"\"\n\t}\n}", sep='')